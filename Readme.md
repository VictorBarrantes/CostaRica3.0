El objetivo principal de este repositorio es recibir la colaboración en forma de ideas  
de todos cuantos deseen que Costa Rica se convierta en un país mejor cada día.  

No pretende ser un grupo político sino analítico que mediante la labor conjunta de todos  
sus colaboradores pueda encontrar soluciones reales y prácticas a los problemas que  
actualmente le impiden a Costa Rica progresar.   

Inicialmente se propone organizar las ideas por areas como se muestra a continuación.  
la idea es montar una estructura de carpetas para ir almacenando las diferentes ideas  
con la finalidad de poder en algún momento generar un documento formal que pueda ser utilizado  
para plantear y concretar proyectos específicos  

Solo a manera de ejemplo inicial:  

#Cultura  
	Música  
	Cine  
	Artes plásticas  
#Economía  
	Hacienda pública  
	Banca estatal  
	Seguros estatales  
	Empresas estatales  
#Educación  
	Básica  
	Intermedia  
	Avanzada  
#Energía  
	Electrica  
		Hidríca  
		Solar  
		Eólica  
		Cinética  
	Combustible  
		Fósiles  
		Bio  
		Hidrógeno  
			líquido  
			plasma (Reactor Stellarator)  
	nuclear  
#Infraestructura  
	Caminos  
	Escuelas  
	Hospitalaria  
	Comunicaciones  
	Deportiva  
#Naturaleza  
	Reservas ecológicas  
	Tratamiendo de resíduos  
	Reciclaje  
	Flora  
	Fauna  
#Producción  
	Agrícola  
	Animal  
	Industrial  
	Comercial  
	Tecnológica  
#Salud  
	Física  
	Laboral  
	Mental  
	Ciudadana  
#Seguridad  
	Social  
	Democrática  
	Jurídica  
	Ciudadana/Civil  
		Emergencias  
			Accidentes  
				Tránsito  
				Laborales  
			Incendios  
				Forestales  
				Domésticos  
			Naturales  
				Sismos  
				Volcanes  
				Pluviales  
				Marinas  
#Tecnología  
	Informática  
	Mecánica  
	Cuántica  
	Nano  
  
Evidentemente falta mucho que agregar al ejemplo, pero justo para eso es este repo.